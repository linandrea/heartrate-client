import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Line } from 'react-chartjs-2';
import { FaHeart, FaHeartbeat } from 'react-icons/fa';

export default function Graph() {
    const tempHrArray = [];
    const tempDateArray = [];
    const tempEpochArray = [];

    const [hrArray, setHrArray] = useState([]);
    const [dateArray, setDateArray] = useState([]);
    const [epochArray, setEpochArray] = useState([]);

    const getDataFromServer = () => {
        axios
			.get(`${process.env.NEXT_PUBLIC_SERVER_URI}/get/25`)
			.then((response) => {
				const responseJson = response['data'];
				responseJson['data'].forEach((element) => {
					tempHrArray.push(element['hr']);
					const currentDate = new Date(element['time']);
					const hours = currentDate.getUTCHours().toString().padStart(2, '0');
					const minutes = currentDate.getUTCMinutes().toString().padStart(2, '0');
					const seconds = currentDate.getUTCSeconds().toString().padStart(2, '0');
					tempDateArray.push(hours + ':' + minutes + ':' + seconds);
					tempEpochArray.push(element['time']);
				});
				tempHrArray.reverse();
				tempDateArray.reverse();
				tempEpochArray.reverse();
				setEpochArray(tempEpochArray);
				setHrArray(tempHrArray);
				setDateArray(tempDateArray);
			})
			.catch((error) => {
				console.error(error);
			});
    };

    useEffect(() => {
        getDataFromServer();
    }, []);

    return (
        <div className="min-w-screen min-h-screen flex items-center justify-center px-5 py-5 bg-gray-200 dark:bg-gray-800">
            <div className="absolute bottom-5 right-5 p-3 rounded-lg bg-white/20 hover:bg-white/10 transition ease-in-out">
                <a href="/">
                    <FaHeartbeat className="text-white" />
                </a>
            </div>
            <div className="rounded shadow-xl overflow-hidden w-full md:flex" style={{ maxWidth: 900 }} x-init="stockTicker.renderChart()">
                <div className="flex w-full md:w-1/2 px-5 pb-4 pt-8 bg-red-400 text-white items-center">
                    <HeartRateChart heartRates={hrArray} timeStamps={dateArray} />
                </div>
                <div className="flex w-full md:w-1/2 p-10 bg-gray-100 text-gray-600 items-center">
                    <DataText heartRates={hrArray} epochArray={epochArray} />
                </div>
            </div>
        </div>
    );
}

function HeartRateChart({ heartRates, timeStamps }) {
    const data = {
        labels: timeStamps,
        datasets: [
            {
                label: 'Heart Rate',
                data: heartRates,
                fill: false,
                backgroundColor: 'rgba(255, 255, 255, 0.1)',
                borderColor: 'rgba(255, 255, 255, 1)',
                pointBackgroundColor: 'rgba(255, 255, 255, 1)',
            },
        ],
    };

    const options = {
        legend: {
            display: false,
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        fontColor: 'rgba(255, 255, 255, 1)',
                    },
                    gridLines: {
                        display: false,
                    },
                },
            ],
            xAxes: [
                {
                    ticks: {
                        fontColor: 'rgba(255, 255, 255, 1)',
                    },
                    gridLines: {
                        color: 'rgba(255, 255, 255, 0.2)',
                        borderDash: [5, 5],
                        zeroLineColor: 'rgba(255, 255, 255, 0.2)',
                        zeroLineBorderDash: [5, 5],
                    },
                },
            ],
        },
    };
    return (
        <>
            <Line data={data} options={options} />
        </>
    );
}

function DataText({ heartRates, epochArray }) {
    const difference = heartRates[heartRates.length - 1] - heartRates[heartRates.length - 2];
    return (
        <div className="w-full">
            <h3 className="text-lg font-semibold leading-tight text-gray-800">Andrea Lin</h3>
            <h6 className="text-sm leading-tight mb-2 text-gray-500">Last update - {new Date(epochArray[epochArray.length - 1]).toGMTString()}</h6>
            <div className="flex w-full items-end mb-6">
                <div className="flex items-baseline text-gray-800">
                    <FaHeart className="text-red-500 text-xl animate-pulse" />
                    <span className="block leading-none text-3xl">&nbsp;{heartRates[heartRates.length - 1]}</span>
                </div>
                {difference >= 0 ? <span className="block leading-5 text-sm ml-4 text-green-500">▲ {difference}</span> : <span className="block leading-5 text-sm ml-4 text-red-500">▼ {difference}</span>}
            </div>
            <div className="flex w-full text-xs text-gray-500">
                <div className="flex w-6/12">
                    <div className="flex-1 pr-3 text-left font-semibold">High</div>
                    <div className="px-3 text-right">{Math.max(...heartRates)}</div>
                </div>
                <div className="flex w-6/12">
                    <div className="flex-1 px-3 text-left font-semibold">Low</div>
                    <div className="pl-3 text-right">{Math.min(...heartRates)}</div>
                </div>
            </div>
            {/*
            <div className="flex w-full text-xs text-gray-500">
                <div className="flex w-6/12">
                    <div className="flex-1 pr-3 text-left font-semibold">Average</div>
                    <div className="px-3 text-right">{average}</div>
                </div>
            </div>
            */}
        </div>
    );
}