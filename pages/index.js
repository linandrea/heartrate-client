import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Head from 'next/head';
import { IconContext } from 'react-icons';
import { FaHeartbeat, FaChartLine } from 'react-icons/fa';

export default function Simple() {
    const [heartRate, setHeartRate] = useState('');
    const [time, setTime] = useState('');

    const getDataFromServer = () => {
        axios
			.get(`${process.env.NEXT_PUBLIC_SERVER_URI}/get/1`)
			.then((response) => {
                console.log(process.env.NEXT_PUBLIC_SERVER_URI);
				const responseJson = response['data'];
				setHeartRate(responseJson['data'][0]['hr']);
				setTime(new Date(responseJson['data'][0]['time']).toLocaleString());
			})
			.catch((error) => {
				console.error(error);
			});
    };

    useEffect(() => {
        getDataFromServer();
    }, []);

    return (
        <>
            {/* Main container */}
            <div className="h-screen relative bg-blue-500 dark:bg-gray-900 flex justify-center items-center overflow-hidden">
                <div className="absolute bottom-5 right-5 p-3 rounded-lg bg-white/20 hover:bg-white/10 transition ease-in-out">
                    <a href="/graph">
                        <FaChartLine className="text-white" />
                    </a>
                </div>
                <span className="animate-ping absolute h-64 w-64 rounded-full bg-white opacity-25 z-0 pulse-delay" />
                <span className="animate-ping absolute h-72 w-72 rounded-full bg-white opacity-25 z-0" />
                <div className="p-5 rounded-full bg-gradient-to-b from-white dark:from-gray-700 to-transparent flex justify-center items-center z-10">
                    {/* Background White Circle */}
                    <div className="p-5 rounded-full bg-white dark:bg-gray-600 flex justify-center items-center">
                        {/* Background Gradient Circle */}
                        <div className="p-32 rounded-full bg-gradient-to-t from-blue-200 dark:from-gray-700 to-transparent flex justify-center items-center text-center flex-col">
                            {/* Content Circle */}
                            <div className="h-1 w-1 rounded-full flex justify-center items-center text-center flex-col">
                                <div className="rounded-full flex justify-center items-center text-center flex-row space-x-1">
                                    <FaHeartbeat className="text-red-600" />
                                    <p className="bpm text-gray-500 dark:text-gray-400">BPM</p>
                                </div>
                                <p className="heartRate dark:text-gray-300 text-8xl">{heartRate}</p>
                                <p className="bpm whitespace-nowrap text-gray-500 dark:text-gray-400">{time}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
